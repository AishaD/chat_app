'use strict'
var Hapi = require('@hapi/hapi');
var Path = require('path')
var http = require('http')
var fs = require('fs'); // required for file serving
// var imageCompression = require('browser-image-compression'); //image compression
var port = process.env.PORT || 4000;
var stringify = require('json-stringify-safe');
var SocketIO = require('socket.io')


const init = async () => {

  const server = Hapi.server({
      port: 3000,
      host: 'localhost',
      routes: {
        files: {
            relativeTo: Path.join(__dirname)
        }
    }
  });
  server.circularRef = server;
  server.list = server;
  

  await server.start();
  console.log('Server running on port 3000');
//  var io = require('socket.io')((http));
  const io = SocketIO.listen(server.listener)


  server.register(require('@hapi/inert'));

  
  server.route({
    method: 'GET',
    path: '/index.html',
    handler: function (request, h) {
  
        return h.file(__dirname + '/index.html');
    }
  });

  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: '.'
        }
    }
});

  
  io.on('connect', (socket) => {
    socket.username = "Anonymouse";
    console.log(socket.username + ' connected on socket ', socket);
    socket.on('change_username', (data) => {
      if (data.username == '') socket.username = 'Anonymouse';
      else socket.username = data.username;
  
      socket.broadcast.emit('chat message', '<span class="username">A new User ' + socket.username + ': Joined The Chatroom </span>');
      console.log("username = " + data.username);
    })
  
    
    socket.on('chat message', (msg) => {
      io.emit('chat message', '<span class="username">' + socket.username + ': </span>' + msg);
    });
  
    socket.on('typing', (data) => {
      socket.broadcast.emit('typing', { username: socket.username });
    })
  
    socket.on('stop typing', () => {
      socket.broadcast.emit('stop typing', {
        username: socket.username
      });
    })
  
  
    //serving images
  
    socket.on('base64 file', function (msg) {
      console.log('received base64 file from' + socket.username);
      //console.log(msg.filename);
      // socket.broadcast.emit('base64 image', //exclude sender
      io.sockets.emit('base64 file',  //include sender
  
        {
          username: socket.username=='' ? 'Anonymouse' : socket.username,
          file: msg.file,
          fileName: msg.fileName
        }
  
      );
    });
 });  
  
  
};

init();

