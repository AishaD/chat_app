'use strict'

exports.route = {
    method: 'GET',
    path: '/home',
    handler: function (request, h) {

        return h.redirect('./index.html');
    }
}